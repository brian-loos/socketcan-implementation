import socket
import pandas as pd
import numpy as np
import time
import can
import RPi.GPIO as GPIO
import os
import multiprocessing as mp

global bus,command_table,s,BUFFER_SIZE
global length_mon,monitors

command_table = {0: ['PIDS_A',False], 1: ['Status',False], 2:['FREEZE_DTC',False], 3:['FUEL_STATUS',False], 4:['ENGINE_LOAD',True,0,100,'percent'],
            5: ['COOLANT_TEMP',True,-10,60,'Celcius'], 6: ['SHORT_FUEL_TRIM_1',True,0,100,'percent'], 7: ['LONG_FUEL_TRIM_1',True,0,100,'percent'],8: ['SHORT_FUEL_TRIM_2',True,0,100,'percent'],
            9: ['LONG_FUEL_TRIM_2',True,0,100,'percent'], 10: ['FUEL_PRESSURE',True,0,1000,'kpa'], 11:['INTAKE_PRESSURE',True,-100,300,'kpa'], 12: ['RPM',True,0,7000,'RPM'],
            13: ['SPEED',True,0,200,'kph'], 14: ['TIMING_ADVANCE',True,-5,25,'degrees'], 15: ['INTAKE_TEMP',True,-10,60,'Celcius'], 16:['MAF',True,0,500,'g/s'], 17: ['THROTTLE_POS',True,0,100,'percent'],
            18: ['AIR_STATUS',False], 19:['O2_SENSORS',False], 20: ['O2_B1S1',True,0,1,'Volts'], 21:[ 'O2_B1S2',True,0,1,'Volts'], 22: ['O2_B1S3',True,0,1,'Volts'],
            23: ['O2_B1S4',True,0,1,'Volts'], 24:['O2_B2S1',True,0,1,'Volts'], 25: ['O2_B2S2',True,0,1,'Volts'], 26: ['O2_B2S3',True,0,1,'Volts'], 27: ['O2_B2S4',True,0,1,'Volts'],
            28: ['OBD_COMPLIANCE',False], 29: ['O2_SENSORS_ALT',False], 30: ['AUX_INPUT_STATUS',False],
            31: ['RUN_TIME',True,0,3600,'seconds'], 32: ['PIDS_B',False], 33: ['DISTANCE_WITH_MIL',True,0,10000,'km'], 34:['FUEL_RAIL_PRESSURE_VAC',True,0,1000,'kpa'],
            35: ['FUEL_RAIL_PRESSURE_DIRECT',True,0,1000,'kpa'], 36:['O2_S2_WR_VOLTAGE',True,0,1,'Volts'],37: ['O2_S2_WR_VOLTAGE',True,0,1,'Volts'],
            38:['O2_S3_WR_VOLTAGE',True,0,1,'Volts'], 39: ['O2_S4_WR_VOLTAGE',True,0,1,'Volts'], 40: ['O2_S5_WR_VOLTAGE',True,0,1,'Volts'],
            41: ['O2_S6_WR_VOLTAGE',True,0,1,'Volts'], 42: ['O2_S7_WR_VOLTAGE',True,0,1,'Volts'], 43:['O2_S8_WR_VOLTAGE',True,0,1,'Volts'],
            44: ['COMMANDED_EGR',True,0,100,'percent'], 45: ['EGR_ERROR',True,0,100,'percent'], 46: ['EVAPORATIVE_PURGE',True,0,100,'percent'], 47: ['FUEL_LEVEL',True,0,100,'percent'],
            48: ['WARMUPS_SINCE_DTC_CLEAR',True,0,100,'count'], 49: ['DISTANCE_SINCE_DTC_CLEAR',True,0,10000,'km'], 50: ['EVAP_VAPOR_PRESSURE',True,-1000,1000,'pa'],
            51: ['BAROMETRIC_PRESSURE',True,50,150,'kpa'], 52: ['O2_S1_WR_CURRENT',True,100,600,'milliamps'], 53: ['O2_S2_WR_CURRENT',True,100,600,'milliamps'],
            54: ['O2_S3_WR_CURRENT',True,100,600,'milliamps'], 55: ['O2_S4_WR_CURRENT',True,100,600,'milliamps'], 56: ['O2_S5_WR_CURRENT',True,100,600,'milliamps'], 57 :['O2_S6_WR_CURRENT',True,100,600,'milliamps'],
            58: ['O2_S7_WR_CURRENT',True,100,600,'milliamps'], 59: ['O2_S8_WR_CURRENT',True,100,600,'milliamps'], 60: ['CATALYST_TEMP_B1S1',True,-10,300,'milliamps'],
            61: ['CATALYST_TEMP_B1S2',True,-10,300,'milliamps'], 62: ['CATALYST_TEMP_B2S1',True,-10,300,'milliamps'], 63: ['CATALYST_TEMP_B2S2',True,-10,300,'milliamps'], 64: ['PIDS_C',False],
            65: ['STATUS_DRIVE_CYCLE',False], 66: ['CONTROL_MODULE_VOLTAGE',True,0,15,'Volt'], 67:[ 'ABSOLUTE_LOAD',True,0,100,'percent'],
            68: ['COMMANDED_EQUIV_RATIO',True,0,23,'ratio'], 69: ['RELATIVE_THROTTLE_POS',True,0,100,'percent'], 70: ['AMBIANT_AIR_TEMP',True,-10,60,'Celcius'],
            71: ['THROTTLE_POS_B',True,0,100,'percent'], 72: ['THROTTLE_POS_C',True,0,100,'percent'], 73: ['ACCELERATOR_POS_D',True,0,100,'percent'],
            74: ['ACCELERATOR_POS_E',True,0,100,'percent'], 75: ['ACCELERATOR_POS_F',True,0,100,'percent'], 76: ['THROTTLE_ACTUATOR',True,0,100,'percent'], 77: ['RUN_TIME_MIL',True,0,3600,'minutes'],
            78: ['TIME_SINCE_DTC_CLEARED',True,0,3600,'minutes'], 79: ['unsupported',False], 80: ['MAX_MAF',True,0,500,'g/s'], 81:['FUEL_TYPE',False],
            82: ['ETHANOL_PERCENT',True,0,100,'percent'], 83: ['EVAP_VAPOR_PRESSURE_ABS',True,0,100,'kpa'], 84: ['EVAP_VAPOR_PRESSURE_ALT',True,0,200,'pa'],
            85: ['SHORT_O2_TRIM_B1',True,0,100,'percent'], 86: ['LONG_O2_TRIM_B1',True,0,100,'percent'], 87: ['SHORT_O2_TRIM_B2',True,0,100,'percent'], 88: ['LONG_O2_TRIM_B2',True,0,100,'percent'],
            89: ['FUEL_RAIL_PRESSURE_ABS',True,0,1000,'kpa'], 90:['RELATIVE_ACCEL_POS',True,0,100,'percent'], 91 : ['HYBRID_BATTERY_REMAINING',True,0,100,'percent'],
            92: ['OIL_TEMP',True,-10,200,'Celcius'], 93: ['FUEL_INJECT_TIMING',True,-5,25,'degrees'], 94:['FUEL_RATE',True,0,10,'L/hr'], 95: ['Emmission requirements',False],96:['PIDs supported[61-80]',False],
            97: ['Drivers demand engine-percent torque', True, 0,100,'percent'],98: ['Actual engine-percent torque',True,0,100,'percent'],
            99:['Engine reference torque',True,0,500,'Nm'],100:['Engine Percent torque data',True,0,100,'percent'],101:['Auxiliary Input/ouput supported',False],
            102:['Mass air flow sensor',True,0,1000,'g/s'],103:['Engine Coolant Temperature',True,-20,200,'Celcius'],104:['Intake Air Temperature',True,-10,70,'Celcius'],
            105:['Commanded EGR and EGR error',True,0,100,'something'],106:['Commanded DIesel intake air flow control and relative intake air flow position',False],
            107:['EGR Temperature',True,-10,300,'Celcius'],108:['Commanded throttle actuator control and relative throttle position',False],
            109:['Fuel Pressure Control System',False], 110:['Injection pressure control system',False],111:['Turbocharger compressor intlet pressure',True,-10,100,'mmhg?'],
            112:['Boost Pressure Control',False], 113:['Variable Geometry turbo control',False],114:['Wastegate Control',False], 115:['Exhaust Pressure',True,0,100,'psi?'],
            116:['Turbocharger RPM',True,0,100000,'rpm'],117:['Turbocharger Temperature',False],118:['Turbocharger Temperature',False],
            119:['Charge air cooler Temperature',True,-10,200,'Celcius'],120:['EGT Bank 1',True,-10,300,'Celcius'],121:['EGT Bank 2',True, -10,300, 'Celcius'],
            122:['Diesel Particulate Filter', False], 123:['Diesel Particulate Filter',False],124:['Diesel Particulate fitler Temperature',False],125:['Nox NTE control area Status',False],
            126:['PM NTE control area Status',False],127:['Engine Run time',True,0,5000,'seconds'],128,['PIDS supported [81-A0]',False],
            129:['Engine run time for AECD',False], 130:['Engine run time for AECD',False], 131:['Nox sensor',True,0,1,'volts'],132:['Manifold Surface Temperature',True,-10,400,'Celcius'],
            133:['Nox reagent system',False],134:['PM sensor',False],135:['Intake manifold absolute pressure',True,-10,100,'mmhg'],136:['SCR induce system',False],
            137:['Run time for AECD',False],138:['Run time for AECD',False],139:['Diesel aftertreatment',False],140:['02 wideband',True,0,1,'volts'],141:['Throttle position G',True, 0,100,'percent'],
            142:['Engine Friction-percent torque',True,-125,130,'percent'],143:['PM sensor bank 1&2',False],144:['WWH-OBD Vehicle OBD system information',False],
            145:['WWH-OBD Vehicle OBD system information',False],146:['Fuel System control',False],147:['WWH-OBD Vehicle OBD system support',False],148:['Nox warning and inducement system',False],
            152:['EGT sensor',True,0,1,'volts'],153:['EGT sensor',True,0,1,'volts'],154:['Hybrid/EV vehicle system data,battery,voltage',False],155:['Diesel Exhaust fluid sensor data',False],
            156:['O2 sensor data',False],157:['Engine Fuel Rate',False],158:['Engine Exhaust flow rate',False],159:['Fuel System Percental Use',True,0,100,'percent'],
            160:['PIDs supported[A1-C0]',False],161:['Nox sensor corrected data',False],162:['Cylinder Fuel Rate',False],163:['Evap System Vapor Pressure',True,0,100,'mmhg'],
            164:['Transmission Actual Gear',True,0,7,'gear'],165:['Diesel Exhaust Fluid Dosing',False],166:['Odometer',True,0,500000,'miles'] }

#set Pi led status
led = 22
GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)
GPIO.setup(led,GPIO.OUT)
GPIO.output(led,True)

#standard message arbitration IDs
PID_REQUEST = 0x7DF
PID_REPLY   = 0x7E8

def establish_can_conn():
    global bus
#establish an OBD connection
    print('attempting to connect to car ')
    #need to perform CAN connection
    print('Bring up CAN0....')
    os.system("sudo /sbin/ip link set can0 up type can bitrate 500000")
    time.sleep(0.1)
    print('Ready')#if this fails we should know
    #setup the PiCAN bus
    try:
	       bus = can.interface.Bus(channel='can0', bustype='socketcan_native')
    except OSError:
	       print('Cannot find PiCAN board.')
	       GPIO.output(led,False)
	       exit()


def setup_connection():
    global s,BUFFER_SIZE
    TCP_IP = '25.17.217.184'
    TCP_PORT = 5005
    BUFFER_SIZE = 640
    s = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
    while 1:
        try:
            time.sleep(.1)
            s.connect((TCP_IP,TCP_PORT))
            break
        except:
            print('waiting for connection')
    print('connected')

def query_PIDs():
    global bus
    #example values, from HONDA PILOT
    print('getting PIDs')
    #need to get the correct ids
    #PIDS_A, PIDS_B, PIDS_C, PIDS_D
    query_PIDs_vals = [0x00,0x20,0x40,0x60,0x80,0xA0]#these are the new PIDs vals
    #PIDS_A = 0x00
    #PIDS_B = 0X20
    #PIDS_C = 0x40
    #PIDS_D = 0x60
    #PIDS_E = 0x80
    #PIDS_F = 0xA0
    #need to send all the PID arbitration requests
    #r1 = connection.query(obd.commands.PIDS_A)
    #r2 = connection.query(obd.commands.PIDS_B)
    #r3 = connection.query(obd.commands.PIDS_C)
    def can_rx_pid_query(PIDs_val,rx_conn,q):#wait to get PID response for PIDs_val
        while True:
            message = bus.recv()
            if message.arbitration_id == PID_REPLY and message.data[2] == PIDs_val:
                supported_pids = message.data[3]#should give us the data.... I'm not sure
                q.put([PIDs_val,supported_pids])#put into the queue
                rx_conn.send('end')
                rx_conn.close()
                break #get out of the loop

    def can_tx_pid_query(PIDs_val,tx_conn):#want to send out a PID query request, only need to break after we know that we get a response
        while True:
            GPIO.output(led,True)
            msg = can.Message(arbitration_id=PID_REQUEST,data=[0x02,0x01,PIDs_val,0x00,0x00,0x00,0x00],extended_id = False)
            bus.send(msg)
            time.sleep(0.1)
            GPIO,output(led,False)
            time.sleep(0.1)
            if tx_conn.poll():#to test if connection has message
                msg = tx_conn.recv()
                if msg == 'end':
                    break

    #suppose we set up pipes between each of the PID channels
    if __name__ = '__main__':
        #this is not the most efficient method to do this. It would be most efficient just to set up each thread to send the data with a
        #then just update the corresponding tag. I should experiment with pipes more
        pipes = []#list of pipes
        recv_processes = [] #recieve processes
        send_processes =[]  #send processes
        pid_vals = []
        q = mp.Queue()
        for vals in query_PIDs_vals:#need to create a set of pipes for these processes and we need to set up all the processes
            tx_conn,rx_conn = mp.Pipe()
            pipes.append([tx_conn,rx_conn])
            recv_processes.append(mp.Process(target = can_rx_pid_query,args =(vals,rx_conn,q)))
            send_processes.append(mp.Process(target = can_tx_pid_query,args =(vals,tx_conn,)))

        #start processes
        for p in recv_processes:
            p.start()
        for p in send_processes:
            p.start()
        #we need to get values from the queue
        time.sleep(1)
        while not q.empty():
            pid_vals.append(q.get())#get the pid values
        #close all the processes
        for p in recv_processes:
            p.join()
        for p in send_processes:
            p.join()

        #no way to know the order in which this will execute, so now we have to sort the resulting queue
        i = 0
        size = len(pid_vals)
        while True:
            if i == size-1:
                break
            elif pid_vals[i][0] > pid_vals[i+1][0]:
                temp = pid_vals[i]
                pid_vals[i] = pid_vals[i+1]
                pid_vals[i+1] = temp
                if not i == 0:
                    i -= 1
            else:
                i += 1
        locs = []
        for vals in pid_vals:
            locs.append(vals[1])#add the values to the list
        print('getting locations')
        sz = np.int32(np.size(locs))
        print('sending data')
        s.sendall(sz.tobytes())
        print(s.recv(BUFFER_SIZE))
        print('sending more shit')
        s.sendall(locs.tobytes())
        print(locs)
        print(s.recv(BUFFER_SIZE))

def restart():
    time.sleep(5)
    s.close()
    setup_connection()

def set_monitors():
    global length_mon,monitors
    length_mon =s.recv(BUFFER_SIZE)
    s.sendall(b'recieved')
    length_mon = (np.frombuffer(length_mon,dtype = np.int32))
    print(length_mon[0])
    monitors = s.recv(BUFFER_SIZE)
    s.sendall(b'recieved')
    monitors = (np.frombuffer(monitors, dtype=np.int32, count = length_mon[0]))



def main():
    global monitors, length_mon,bus
    prev_vals = np.zeros(np.size(monitors))
    t0 = time.time()
    #this is the main loop

    while 1:
        #print('eneter value to send: ')
        #val = input()
        #print('sending ',val)
        #val = np.int64(val)
        #generate random values
        #val = np.random.randint(-10,10,(10,1),dtype = np.int64) #random vector
        queue = []
        i = 0

        for monitor in monitors: #need to update values
            #commented out stuff we need to run on car
            r = connection.query(obd.commands[1][monitor])
            #r = query_val(monitor)#to subroutine to query value from CAN bus
            time.sleep(.003)
            if not r.is_null():
                queue.append(np.single(r.value.magnitude))#query the value
            else:
                queue.append(prev_vals[i])#most recently seend value
            i += 1
        #queue should be populated now
        queue = np.array(queue)
        prev_vals = queue
        t  = time.time() #get current time
        t= t-t0 #increase the resolution
        queue = np.append(queue, np.single(t))
        #now last value is time
        s.sendall(queue.tobytes())
        data = s.recv(BUFFER_SIZE)
def close_all():
    s.close()
    #connection.stop()
    #connection.close()



#first establish obd connection
#establish_obd_conn()
#setup TCP connection
setup_connection()
time.sleep(1)
count = 0
while 1:
    time.sleep(1)
    try:
        msg = s.recv(BUFFER_SIZE)
        if not(msg == b''): print(msg)
        if msg == b'get_PIDs':
            query_PIDs()
        elif msg == b'set_mon':
            print('setting monitors')
            s.sendall(b'starting')
            set_monitors()
        elif msg == b'close_all':
            close_all()
        elif msg == b'restart':
            restart()
        elif msg == b'set_mon':
            set_monitors()
        elif msg == b'query':
            print('main query')
            time.sleep(10)
            main()
    except:
        print('no command')
        count += 1
        if count > 10:
            restart()#assume connection was lost
