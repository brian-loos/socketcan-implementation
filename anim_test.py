#we want to test multiprocessing with basic output plotting update, and generating new values in the plots/display
#this works so far for plotting on the main thread, we could set up a shared memory space to try putting the plotting
#thread on to a seperate thread, but matplotlib will likely not appreciate that
import socket
import pandas as pd
import numpy as np
import time
from matplotlib import pyplot as plt
#import can
#import RPi.GPIO as GPIO
import os
import multiprocessing as mp
global lines, backgrounds,fig,axs
import sys
import matplotlib
from PyQt5 import QtCore, QtWidgets
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg
from matplotlib.figure import Figure
from matplotlib.backends.backend_qt5agg import FigureCanvas
import matplotlib.animation as anim

def gen_value(vals,q):
    while True:
        t_sleep = np.random.rand() #generate random sleep time
        time.sleep(t_sleep/4)#randomly wait
        val = np.random.randint(0,10)
        #print("from thread "+str(vals)+" value " + str(val))
        q.put([vals,val])#send id and value

def update_canvas():
    for i in range(num_vals):
        self.vals_arr.append([])
    recv_vals = []
    while not q.empty():
        recv_vals.append(q.get())
    for val in recv_vals:
        i = val[0]
        x = val[1]
        vals_arr[i].append(x)

    max_len = 100
    for i in range(num_vals):
        if len(vals_arr[i]) > 100:
            diff = len(vals_arr[i])-100
            vals_arr[i] = vals_arr[i][diff:]

        lines.set_data(range(len(vals_arr[i])),vals_arr[i])
    return lines


if __name__ == "__main__":
    pipes = []
    procs = []  #send processes

    q = mp.Queue()
    num_vals = 4

    for vals in range(num_vals):#need to create a set of pipes for these processes and we need to set up all the processes
        #don't need pipes for this implementation
        #tx_conn,rx_conn = mp.Pipe()
        #pipes.append([tx_conn,rx_conn])
        procs.append(mp.Process(target = gen_value,args =(vals,q)))

    #start processes
    for p in procs:
        p.start()

    #we need to get values from the queue
    time.sleep(1)# let the program start
    figure = Figure()
    axs = figure.subplots(num_vals,1)
    lines = []
    for i in range(num_vals):
        lines.append(axs[i].plot([],lw =1))
        crit_val = np.random.randint(3,8)
        axs[i].plot([0,99],[crit_val,crit_val],color = 'red')
        axs[i].set_ylim(0,10)
        axs[i].set_xlim(0,100)

    interval = .05
    vals_arr = []
    anim.FuncAnimation(fig = figure, func = update_canvas, fargs = (), interval = interval , blit = True, repeat = False)
    plt.show()

    input("press a key then enter to exit")
    #close all the processes
    for p in procs:
        p.terminate()
        p.join()
