#####
#This is the first iteration of a GUI front end to run on a pi in some hypothetical car using pyCan through socketCAN
#This uses a Qt5 front end running realtime matplotlib animations to plot realtime incoming data from a data buffer handled by socketCAN



###############___________________________________________________
import socket
import pandas as pd
import numpy as np
import time
from matplotlib import pyplot as plt
import can
#import RPi.GPIO as GPIO #NEED THIS LINE FOR RPI VERSION
import os
import multiprocessing as mp
global lines, backgrounds,fig,axs
import sys
import matplotlib
from PyQt5 import QtCore, QtWidgets
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg
from matplotlib.figure import Figure
from matplotlib.backends.backend_qt5agg import FigureCanvas
import matplotlib.animation as anim
global q,num_vals, command_table, active_mons,bus


active_mons = []

command_table = {0: ['PIDS_A',False], 1: ['Status',False], 2:['FREEZE_DTC',False], 3:['FUEL_STATUS',False], 4:['ENGINE_LOAD',True,0,100,'percent'],
            5: ['COOLANT_TEMP',True,-10,60,'Celcius'], 6: ['SHORT_FUEL_TRIM_1',True,0,100,'percent'], 7: ['LONG_FUEL_TRIM_1',True,0,100,'percent'],8: ['SHORT_FUEL_TRIM_2',True,0,100,'percent'],
            9: ['LONG_FUEL_TRIM_2',True,0,100,'percent'], 10: ['FUEL_PRESSURE',True,0,1000,'kpa'], 11:['INTAKE_PRESSURE',True,-100,300,'kpa'], 12: ['RPM',True,0,7000,'RPM'],
            13: ['SPEED',True,0,200,'kph'], 14: ['TIMING_ADVANCE',True,-5,25,'degrees'], 15: ['INTAKE_TEMP',True,-10,60,'Celcius'], 16:['MAF',True,0,500,'g/s'], 17: ['THROTTLE_POS',True,0,100,'percent'],
            18: ['AIR_STATUS',False], 19:['O2_SENSORS',False], 20: ['O2_B1S1',True,0,1,'Volts'], 21:[ 'O2_B1S2',True,0,1,'Volts'], 22: ['O2_B1S3',True,0,1,'Volts'],
            23: ['O2_B1S4',True,0,1,'Volts'], 24:['O2_B2S1',True,0,1,'Volts'], 25: ['O2_B2S2',True,0,1,'Volts'], 26: ['O2_B2S3',True,0,1,'Volts'], 27: ['O2_B2S4',True,0,1,'Volts'],
            28: ['OBD_COMPLIANCE',False], 29: ['O2_SENSORS_ALT',False], 30: ['AUX_INPUT_STATUS',False],
            31: ['RUN_TIME',True,0,3600,'seconds'], 32: ['PIDS_B',False], 33: ['DISTANCE_WITH_MIL',True,0,10000,'km'], 34:['FUEL_RAIL_PRESSURE_VAC',True,0,1000,'kpa'],
            35: ['FUEL_RAIL_PRESSURE_DIRECT',True,0,1000,'kpa'], 36:['O2_S2_WR_VOLTAGE',True,0,1,'Volts'],37: ['O2_S2_WR_VOLTAGE',True,0,1,'Volts'],
            38:['O2_S3_WR_VOLTAGE',True,0,1,'Volts'], 39: ['O2_S4_WR_VOLTAGE',True,0,1,'Volts'], 40: ['O2_S5_WR_VOLTAGE',True,0,1,'Volts'],
            41: ['O2_S6_WR_VOLTAGE',True,0,1,'Volts'], 42: ['O2_S7_WR_VOLTAGE',True,0,1,'Volts'], 43:['O2_S8_WR_VOLTAGE',True,0,1,'Volts'],
            44: ['COMMANDED_EGR',True,0,100,'percent'], 45: ['EGR_ERROR',True,0,100,'percent'], 46: ['EVAPORATIVE_PURGE',True,0,100,'percent'], 47: ['FUEL_LEVEL',True,0,100,'percent'],
            48: ['WARMUPS_SINCE_DTC_CLEAR',True,0,100,'count'], 49: ['DISTANCE_SINCE_DTC_CLEAR',True,0,10000,'km'], 50: ['EVAP_VAPOR_PRESSURE',True,-1000,1000,'pa'],
            51: ['BAROMETRIC_PRESSURE',True,50,150,'kpa'], 52: ['O2_S1_WR_CURRENT',True,100,600,'milliamps'], 53: ['O2_S2_WR_CURRENT',True,100,600,'milliamps'],
            54: ['O2_S3_WR_CURRENT',True,100,600,'milliamps'], 55: ['O2_S4_WR_CURRENT',True,100,600,'milliamps'], 56: ['O2_S5_WR_CURRENT',True,100,600,'milliamps'], 57 :['O2_S6_WR_CURRENT',True,100,600,'milliamps'],
            58: ['O2_S7_WR_CURRENT',True,100,600,'milliamps'], 59: ['O2_S8_WR_CURRENT',True,100,600,'milliamps'], 60: ['CATALYST_TEMP_B1S1',True,-10,300,'milliamps'],
            61: ['CATALYST_TEMP_B1S2',True,-10,300,'milliamps'], 62: ['CATALYST_TEMP_B2S1',True,-10,300,'milliamps'], 63: ['CATALYST_TEMP_B2S2',True,-10,300,'milliamps'], 64: ['PIDS_C',False],
            65: ['STATUS_DRIVE_CYCLE',False], 66: ['CONTROL_MODULE_VOLTAGE',True,0,15,'Volt'], 67:[ 'ABSOLUTE_LOAD',True,0,100,'percent'],
            68: ['COMMANDED_EQUIV_RATIO',True,0,23,'ratio'], 69: ['RELATIVE_THROTTLE_POS',True,0,100,'percent'], 70: ['AMBIANT_AIR_TEMP',True,-10,60,'Celcius'],
            71: ['THROTTLE_POS_B',True,0,100,'percent'], 72: ['THROTTLE_POS_C',True,0,100,'percent'], 73: ['ACCELERATOR_POS_D',True,0,100,'percent'],
            74: ['ACCELERATOR_POS_E',True,0,100,'percent'], 75: ['ACCELERATOR_POS_F',True,0,100,'percent'], 76: ['THROTTLE_ACTUATOR',True,0,100,'percent'], 77: ['RUN_TIME_MIL',True,0,3600,'minutes'],
            78: ['TIME_SINCE_DTC_CLEARED',True,0,3600,'minutes'], 79: ['unsupported',False], 80: ['MAX_MAF',True,0,500,'g/s'], 81:['FUEL_TYPE',False],
            82: ['ETHANOL_PERCENT',True,0,100,'percent'], 83: ['EVAP_VAPOR_PRESSURE_ABS',True,0,100,'kpa'], 84: ['EVAP_VAPOR_PRESSURE_ALT',True,0,200,'pa'],
            85: ['SHORT_O2_TRIM_B1',True,0,100,'percent'], 86: ['LONG_O2_TRIM_B1',True,0,100,'percent'], 87: ['SHORT_O2_TRIM_B2',True,0,100,'percent'], 88: ['LONG_O2_TRIM_B2',True,0,100,'percent'],
            89: ['FUEL_RAIL_PRESSURE_ABS',True,0,1000,'kpa'], 90:['RELATIVE_ACCEL_POS',True,0,100,'percent'], 91 : ['HYBRID_BATTERY_REMAINING',True,0,100,'percent'],
            92: ['OIL_TEMP',True,-10,200,'Celcius'], 93: ['FUEL_INJECT_TIMING',True,-5,25,'degrees'], 94:['FUEL_RATE',True,0,10,'L/hr'], 95: ['Emmission requirements',False],96:['PIDs supported[61-80]',False],
            97: ['Drivers demand engine-percent torque', True, 0,100,'percent'],98: ['Actual engine-percent torque',True,0,100,'percent'],
            99:['Engine reference torque',True,0,500,'Nm'],100:['Engine Percent torque data',True,0,100,'percent'],101:['Auxiliary Input/ouput supported',False],
            102:['Mass air flow sensor',True,0,1000,'g/s'],103:['Engine Coolant Temperature',True,-20,200,'Celcius'],104:['Intake Air Temperature',True,-10,70,'Celcius'],
            105:['Commanded EGR and EGR error',True,0,100,'something'],106:['Commanded DIesel intake air flow control and relative intake air flow position',False],
            107:['EGR Temperature',True,-10,300,'Celcius'],108:['Commanded throttle actuator control and relative throttle position',False],
            109:['Fuel Pressure Control System',False], 110:['Injection pressure control system',False],111:['Turbocharger compressor intlet pressure',True,-10,100,'mmhg?'],
            112:['Boost Pressure Control',False], 113:['Variable Geometry turbo control',False],114:['Wastegate Control',False], 115:['Exhaust Pressure',True,0,100,'psi?'],
            116:['Turbocharger RPM',True,0,100000,'rpm'],117:['Turbocharger Temperature',False],118:['Turbocharger Temperature',False],
            119:['Charge air cooler Temperature',True,-10,200,'Celcius'],120:['EGT Bank 1',True,-10,300,'Celcius'],121:['EGT Bank 2',True, -10,300, 'Celcius'],
            122:['Diesel Particulate Filter', False], 123:['Diesel Particulate Filter',False],124:['Diesel Particulate fitler Temperature',False],125:['Nox NTE control area Status',False],
            126:['PM NTE control area Status',False],127:['Engine Run time',True,0,5000,'seconds'],128:['PIDS supported [81-A0]',False],
            129:['Engine run time for AECD',False], 130:['Engine run time for AECD',False], 131:['Nox sensor',True,0,1,'volts'],132:['Manifold Surface Temperature',True,-10,400,'Celcius'],
            133:['Nox reagent system',False],134:['PM sensor',False],135:['Intake manifold absolute pressure',True,-10,100,'mmhg'],136:['SCR induce system',False],
            137:['Run time for AECD',False],138:['Run time for AECD',False],139:['Diesel aftertreatment',False],140:['02 wideband',True,0,1,'volts'],141:['Throttle position G',True, 0,100,'percent'],
            142:['Engine Friction-percent torque',True,-125,130,'percent'],143:['PM sensor bank 1&2',False],144:['WWH-OBD Vehicle OBD system information',False],
            145:['WWH-OBD Vehicle OBD system information',False],146:['Fuel System control',False],147:['WWH-OBD Vehicle OBD system support',False],148:['Nox warning and inducement system',False],
            152:['EGT sensor',True,0,1,'volts'],153:['EGT sensor',True,0,1,'volts'],154:['Hybrid/EV vehicle system data,battery,voltage',False],155:['Diesel Exhaust fluid sensor data',False],
            156:['O2 sensor data',False],157:['Engine Fuel Rate',False],158:['Engine Exhaust flow rate',False],159:['Fuel System Percental Use',True,0,100,'percent'],
            160:['PIDs supported[A1-C0]',False],161:['Nox sensor corrected data',False],162:['Cylinder Fuel Rate',False],163:['Evap System Vapor Pressure',True,0,100,'mmhg'],
            164:['Transmission Actual Gear',True,0,7,'gear'],165:['Diesel Exhaust Fluid Dosing',False],166:['Odometer',True,0,500000,'miles'] }

#to access values of the command table, command_table[loc][0]--> name
#                                       command_table[loc][1]--> whether or not it has a value
#                                       command_table[loc][2]--> min ranges
#                                       command_table[loc][3]--> max range
#                                       command_table[loc][4]--> units
matplotlib.use('Qt5Agg')
#working framework for plotting line animations
class AnimFigureCanvas(FigureCanvasQTAgg, anim.FuncAnimation):
    #This class takes in an active queue, q (from multiprocessing) and the number of values that are going to be plotted
    #we can add another way to specify the location from which the values are coming from
    def __init__(self,q,monitors)-> None:
        super().__init__(Figure())
        self._q_ = q #need access to the queue
        self._num_vals_ = len(monitors)#this is the same but we shouldn't use it

        self._monitors_ = monitors#these are the keys which we care about
        print(self._monitors_,self._num_vals_)
        self.axs = self.figure.subplots(self._num_vals_,1)

        self._lines_ = []
        #need to update this to accomodate for different sources
        for i in range(self._num_vals_):
            loc = self._monitors_[i]
            self._lines_.append(self.axs[i].plot([],lw = 1)[0])
            crit_val = np.random.rand()
            self.axs[i].plot([0,99],[crit_val,crit_val], color = 'red')
            self.axs[i].set_ylim(command_table[loc][2],command_table[loc][3])
            self.axs[i].set_xlim(0,10)
            self.axs[i].set_title(command_table[loc][0],loc = 'right')
            self.axs[i].set_xlabel(command_table[loc][4])
            self.axs[i].set_xticks(list(range(0,10)))

            self.axs[i].set_xticklabels(list(range(-10,0)))


        interval = .01# we want to update as quick as possible, probably
        self.vals_arr = []
        self._times_ = []
        for i in range(self._num_vals_):
            self.vals_arr.append([.5])#init value
            self._times_.append([0.])
        #self._timer_ = self.new_timer(interval, [(self._update_canvas_, (), {})])
        #self._timer_.start()
        #self._tstart_ = time.time()#start time
        #anim.FuncAnimation.__init__(self,self.figure, self._update_canvas_, fargs=(), interval=interval, blit=True,repeat = False)
        return

    def start_anim(self):
        print('starting animation')
        self._tstart_ = time.time()#start time
        interval = .01# we want to update as quick as possible, probably
        anim.FuncAnimation.__init__(self,self.figure, self._update_canvas_, fargs=(), interval=interval, blit=True,repeat = False)

    def _update_canvas_(self,i)-> None:
        #This is the update method for the animation framework running the graph
        #get current time
        num_new_vals = np.zeros(self._num_vals_) #init to zero
        recv_vals = []
        while not self._q_.empty():
            recv_vals.append(self._q_.get())#get the values and append to a list
            #get a copy of the current queue
        #need to make sure that the values that we recieve are values that we want to track
        #suppose that recieved messages have the format [id, value]

        for i in range(self._num_vals_):
            num_new_vals[i] = len(self.vals_arr[i])#init value
        for val in recv_vals:
            id = val[0]
            x= val[1]
            #need to actively filter out the elements which we are not tracking
            #we need to check to see if id is in self._monitors_
            contains = False
            for i in range(self._num_vals_):
                if self._monitors_[i] == id:
                    contains = True
                    id = i#set value to the correct plot
                    break

            print('in animation ',x,id)
            if contains:
                #print('contains',i)
                #self.vals_arr[i].append(x+ self.vals_arr[i][len(self.vals_arr[i])-1])
                self.vals_arr[id].append(x)
            else: #value is not for plotting, then put back into queue
                self._q_.put(val)#put back onto queue for someone else to get
        self._tcurr_ = time.time()-self._tstart_#get current time
        for i in range(self._num_vals_):
            num_new_vals[i] = len(self.vals_arr[i])-num_new_vals[i]
            #need to know if this is nonzero
            if num_new_vals[i] != 0:
                end_val = self._times_[i][len(self._times_[i])-1]
                dt = self._tcurr_-end_val
                self._times_[i].extend(list(np.linspace(end_val+dt/num_new_vals[i], self._tcurr_, int(num_new_vals[i]))))#add values to times

        cutoff = 10
        max_len = 10000
        for i in range(self._num_vals_):
            if self._times_[i][len(self._times_[i])-1] > cutoff:
                diff = int(num_new_vals[i])
                self.vals_arr[i] = self.vals_arr[i][diff:]
                self._times_[i] = self._times_[i][diff:]
                self.axs[i].set_xlim(self._times_[i][0],self._times_[i][len(self._times_[i])-1])
            #    self.axs[i].set_xticklabels(list(np.linspace(self._times_[i][0],self._times_[i][len(self._times_[i])-1],10)))
            #self.axs[i].clear()
            #self.axs[i].set_ylim(0,10)
            #self.axs[i].set_xlim(0,100)
            #self.axs[i].plot(range(len(self.vals_arr[i])),self.vals_arr[i])
            #print(self._lines_[i])
            #print(self._lines_[i][0])
            self._lines_[i].set_xdata(self._times_[i])
            self._lines_[i].set_ydata(self.vals_arr[i])
        self._t0_ = time.time()
        #self.draw()
        #self.show()
        return self._lines_
#working framework for plotting polar animations (gauge clusters, rpm, anything else where this might be useful)
class PolarFigureCanvas(FigureCanvasQTAgg, anim.FuncAnimation):
    #This class takes in an active queue, q (from multiprocessing) and the number of values that are going to be plotted
    #we can add another way to specify the location from which the values are coming from
    def __init__(self)-> None:
        super().__init__(Figure())

        self.axs = self.figure.subplots(subplot_kw = {'projection':'polar'})
        self._lines_ = []

        self._theta_ = 0
        self._lines_.append(self.axs.plot([],lw = 4)[0])
        self._lines_[0].set_data([0,self._theta_],[0,2])
        self.axs.set_rmax(3)
        self.axs.set_rticks([1,2,3])
        self.axs.fill_between(np.linspace(np.pi, np.pi*3/2, 100),0,3,)

        interval = .01# we want to update as quick as possible, probably
        anim.FuncAnimation.__init__(self,self.figure, self._update_canvas_, fargs=(), interval=interval, blit=True,repeat = False)
        return
    def _update_canvas_(self,i)-> None:
        self._theta_ = self._theta_ + self.get_next_val(i)
        self._lines_[0].set_data([0,self._theta_],[0,2])
        return self._lines_
    def get_next_val(self,i):
        return (np.random.rand()-.5)*.05

class pids_selection(QWidget):
    def __init__(self):
        super().__init__()
        self.layout = QVBoxLayout()
        #need to create a checkbox for each PID value
        self.boxes = []
        self.locs = []

        i = 0
        for item in command_table.values():
            if item[1] == True:
                name = item[0]#the name of the PIDs
                temp = QCheckBox(name)
                temp.setChecked(False)
                temp.setDisabled(True)
                temp.setFont(QFont('Lucidia Console', 16))
                self.boxes.append(temp)
                self.layout.addWidget(self.boxes[i],i)
                i += 1

        self.setLayout(self.layout)

    def update_pids(self,locs):
        for box in self.boxes:
            self.layout.removeWidget(box)
            box.deleteLater()
            box = None
        self.boxes = []
        i=-1
        self.locs = locs
        for loc in locs:
            print(loc)
            item = command_table[loc]
            name = item[0]
            if item[1] == True:
                i+=1
                temp = QCheckBox(name)
                temp.setChecked(False)
                temp.setFont(QFont('Lucidia Console', 16))
                self.boxes.append(temp)
                self.boxes[i].stateChanged.connect(lambda checked, a = i:self.chkstate(self.boxes[a]))
                self.layout.addWidget(self.boxes[i],i)


    def chkstate(self,b):
        for loc in self.locs:
            item = command_table[loc]
            name = item[0]
            if name == b.text():
                if b.isChecked():
                    #need to check if we have too many
                    active_mons.append(loc)

                elif (loc in monitors):
                    active_mons.remove(loc)
                #print(b.text())
                #print(monitors)


#class for the main program, this needs to be updated to include more features
#currently it sets up so that it is a fullscreen application
#it should take in the working queue and screen resolution as variables and everything else should be handled within the UI
class MainWindow(QtWidgets.QMainWindow):

    def __init__(self,q,signal_q, num_vals,screen_width, screen_height,monitors ,*args, **kwargs):
        super(MainWindow, self).__init__(*args, **kwargs)
        sizeObject = QDesktopWidget().screenGeometry(-1)
        self.q = q
        self._signal_q_ = signal_q
        self.num_vals = num_vals
        self._width_ = screen_width
        self._height_ = screen_height-100
        self._mons_ = monitors

        self.setupUi()
        # Create the maptlotlib FigureCanvas object,
        # which defines a single set of axes as self.axes.
        self.show()
    def setupUi(self):
        self.setWindowTitle('Plotting thingy')
        #test program for the moment
        self.resize(self._width_, self._height_)
        self.move(0,0)
        wid = QWidget(self) #create central widget
        self.setCentralWidget(wid)
        self.layout = QGridLayout()#create the grid layout
        wid.setLayout(self.layout)#this layout is the layout for our program
        push_button = QPushButton()
        push_button.setText('fuck you')
        push_button.setFont(QFont('Lucidia Console', 16))
        push_button.clicked.connect(self.fuck_you)
        self.layout.addWidget(push_button,0,0)
        get_pids = QPushButton()
        get_pids.setText('get PIDs')
        get_pids.setFont(QFont('Lucidia Console', 16))
        get_pids.clicked.connect(self.get_supported_pids)
        self.layout.addWidget(get_pids,1,0)
        start_plot = QPushButton()
        start_plot.setText('Start Plotting')
        start_plot.setFont(QFont('Lucidia Console', 16))
        start_plot.clicked.connect(self.initialize_plotting)

        self.layout.addWidget(start_plot,3,0)
        #sc = MplCanvas(self, self.q, width=5, height=4, dpi=100,num_vals = 4)
        #rp = PolarFigureCanvas()
        #sc = TestClass()
        #need to run a modified version of the plotting method
        #need to start the animation after choosing which values to track
        #layout.addWidget(rp,1,0,2,1)
        self.show()
        #sc.run(4,self.q)

        pids_scroll = QScrollArea()
        self.checkable_pids = pids_selection()
        pids_scroll.setWidget(self.checkable_pids)
        pids_scroll.setWidgetResizable(True)
        #pids_scroll.setFixedHeight(250)
        pids_scroll.setFixedWidth(self._width_//3)

        self.layout.addWidget(pids_scroll, 2,0)

    def fuck_you(self):
        #text, ok = QInputDialog.getText(self, 'Fuck you', 'Big fuck you') #this is a interactive text box
        self._signal_q_.put(['dicks', 'more dicks']) #send a control singal to another thread

    def get_supported_pids(self):#this will fetch the current available pids
        self._signal_q_.put(['get_pids','query'])
        time.sleep(3)
        t_start = time.time()
        #need to create or do an inplace sort
        PID_list = []
        self._mons_ = []
        query_PIDs_vals = [0x00,0x20,0x40,0x60,0x80,0xA0]
        while True:#wait to recieve values
            t = time.time()
            if t-t_start > 10:
                print('timed out getting PIDs, returning fetched PIDs')
                break
            if not q.empty():
                msg = q.get()
                val = msg[0]
                state = msg[1]
                if val in query_PIDs_vals:#check if
                    PID_list.append([val,state])#put into temp array, each message should only be sent once, so this will work
                else:
                    q.put(msg)#put the message back into the queue, maybe we won't get infinite loop now
                    break
            if len(PID_list) >= len(query_PIDs_vals):
                break
        #now we need to sort the values that we have because we do not know the order which we will recieve them
        for i in range(len(query_PIDs_vals)):
            for PID,val in PID_list:
                if PID == query_PIDs_vals[i]:
                    #THIS MAY NEED TO BE MODIFIED
                    #I am not sure what the response type is from the CAN network
                    self._mons_.extend(val)



        print(self._mons_)
        self.checkable_pids.update_pids(locs = self._mons_)
        print('got locs')

    def initialize_plotting(self): #we need to get a list of which PIDs are checked in the scrollable box then start plotting which ever things are checked
        print(active_mons)#debugging
        #supposing active_mons is our list with the PIDs that we are wanting to track
        #we need to send a signal to the _threadManager_ to start threads to put live data into the queue
        for mon in active_mons:
            self._signal_q_.put([mon,'scan'])#this signal should tell us to set up a thread as a live watcher
        time.sleep(.5)#to wait to make sure that we have all threads running
        #the generated threads should be generating data and putting them on the queue, now we just need to initalize the plots correctly

        #start the plot with the current values which are being scanned
        sc = AnimFigureCanvas(self.q, active_mons)
        self.layout.addWidget(sc,0,1,4, 3)
        sc.start_anim()#start the animation



num_vals = 3 #depricated

#thread management
#need to add functionality for killing PID query threads which may prove difficult wihtout setting up more pipes
def thread_Manager(q,signal_q):
    procs = []  #send processes
    #current test processes
    #for vals in range(num_vals):#need to create a set of pipes for these processes and we need to set up all the processes
    #    procs.append(mp.Process(target = gen_value,args =(vals,q)))

    #for p in procs:
    #    p.start()
    rx_procs = []
    tx_procs = []
    while True: #run constantly
        if not signal_q.empty(): #create or remove a new thread,
        #follow the following format [val,state], val is PID, state is scan, stop, query
            msg = signal_q.get()
            val = msg[0]
            state = msg[1]
            print(val,state)
            if state == 'query':
                if val == 'get_pids':
                    #code to query for all the desired PIDs
                    query_PIDs_vals = [0x00,0x20,0x40,0x60,0x80,0xA0]#these are PID values for querying which PID values are supported by the CAN network

                    #sets up a thread to query PID to get responses, then we want to wait for all of these processes to end
                    #these threads will put their responses onto the main queue, q, not signal_q
                    for val in query_PIDs_vals: #set up all the pipes and start processes
                        rx_conn,tx_conn = mp.Pipe()
                        tx_p = mp.Process(target = can_tx_pid_query,args = (val,tx_conn))
                        rx_p = mp.Process(target = can_rx_pid_query,args = (val,rx_conn,q))#start
                        tx_p.start()
                        rx_p.start()

                    #wait for all threads to finish
                    for p in rx_procs:
                        p.join()
                    for p in tx_procs:
                        p.join()

                    #print('fuci')
                    #signal_q.put(['ret_pids',[23,34,57,89,100]])
            elif state == 'scan': #tells us that we want to set up a thread to scan for values associated with a certain PID
                #need to check of tracked values contains the PID to be tracked
                contains = False
                for p in procs:
                    if p.name == str(val):
                        contains = True
                        break
                if not contains:
                    p = mp.Process(target = scan_PID, args = (val,q))
                    p.name = str(val)
                    p.start()
                    procs.append(p)
            elif state == 'stop':
                for p in procs:
                    if p.name == str(val):
                        procs.remove(p)#remove from list
                        p.terminate()
                        p.join()


            else:
                signal_q.put(msg)
            #given a value and state, this tells us what we want to do with it


#basically need to add a method here to fetch values using socketCAN rather than generating random values
def gen_value(vals,q):
    while True:
        t_sleep = np.random.rand() #generate random sleep time
        time.sleep(t_sleep/20)#randomly wait
        #val = np.random.randint(0,10)
        val = (np.random.rand() -.5)/25#small value
        #print("from thread "+str(vals)+" value " + str(val))
        q.put([vals,val])#send id and value


#this method must call the tx and rx query functions to constantly search for PIDs, then have them get put onto the bus

def scan_PID(PID,q):#scans CANBUS for PID value and returns query values into the queue
    while True:
        time.sleep(.1)
        val = np.random.random() #generate a random value for now that is within the limits of value which are acceptable
        #need to create functions to communicate over socketCAN to talk to the car

        val = val*(command_table[PID][3]-command_table[PID][2])+command_table[PID][2]
        q.put([PID,val])
        #print(PID, val)

#establishes a CAN bus connection and defines the global variable bus (which is the bus that we are communicating on)
def establish_can_conn():
    global bus
    #establish an OBD connection
    print('attempting to connect to car ')
    #need to perform CAN connection
    print('Bring up CAN0....')
    os.system("sudo /sbin/ip link set can0 up type can bitrate 500000")
    time.sleep(0.1)
    print('Ready')#if this fails we should know
    #setup the PiCAN bus
    try:
	       bus = can.interface.Bus(channel='can0', bustype='socketcan_native')
    except OSError:
	       print('Cannot find PiCAN board.')
	       GPIO.output(led,False)
	       exit()

#these functions must be created on threads in pairs which respective pipes connecting them for now, otherwise we
#always will attempt to put the same message onto the bus

#this constantly scans the CAN network for PID replies, then puts those replies onto a Queue
#so for processing by the main thread
def can_rx_pid_query(PIDs_val,rx_conn,q):#wait to get PID response for PIDs_val
    while True:
        message = bus.recv()
        if message.arbitration_id == PID_REPLY and message.data[2] == PIDs_val:
            supported_pids = message.data[3]#should give us the data.... I'm not sure
            q.put([PIDs_val,supported_pids])#put into the queue
            rx_conn.send('end')
            rx_conn.close()
            break #get out of the loop

#this constantly puts a PID query request on the CAN bus until a reply is heard by the other thread, then
#the other thread sends a stop request which stops the querying
def can_tx_pid_query(PIDs_val,tx_conn):#want to send out a PID query request, only need to break after we know that we get a response
    while True:
        GPIO.output(led,True)
        msg = can.Message(arbitration_id=PID_REQUEST,data=[0x02,0x01,PIDs_val,0x00,0x00,0x00,0x00],extended_id = False)#standard message format for PID query
        bus.send(msg)
        time.sleep(0.1)
        GPIO,output(led,False)
        time.sleep(0.1)
        if tx_conn.poll():#to test if connection has message
            msg = tx_conn.recv()
            if msg == 'end':
                break

if __name__ == "__main__":
    #stuff for setting up CAN connection
    led = 22
    GPIO.setmode(GPIO.BCM)
    GPIO.setwarnings(False)
    GPIO.setup(led,GPIO.OUT)
    GPIO.output(led,True)

    #standard message arbitration IDs
    PID_REQUEST = 0x7DF#may need to put these into the global scope as well
    PID_REPLY   = 0x7E8

    establish_can_conn()#start a CAN connection and establish bus

    #there are the set of values which must be queried when querying the BUS to find out which
    #PIDs are supported by the network
    query_PIDs_vals = [0x00,0x20,0x40,0x60,0x80,0xA0]#these are PID values for querying which PID values are supported by the CAN network

    #data queue
    q = mp.Queue()
    #thread management queue
    signal_q = mp.Queue()

    #create a thread manager for handling creating more threads and what not
    _threadManager_ = mp.Process(target = thread_Manager, args = (q,signal_q,))
    #procs.append(_threadManager_)
    _threadManager_.start()
    #test threads for generating values for the random plots for now, this is depricated
    #need to first poll the ECU to find which PID monitors are supported by the ECU this can be done quickly probably
    #suppose it has the format of a list of numbers representing which values are supported
    test_mons = [23,34,57,89,100]# suppose there are the available monitors
    all_monitors = list(command_table.keys())#gets all the PID values as decimal values
    #we need to get values from the queue
    time.sleep(1)# let the program start
    app = QtWidgets.QApplication(sys.argv)
    screen = app.primaryScreen()
    size = screen.size()
    #in general, we may be better off sending an entire list of all monitors instead
    w = MainWindow(q = q,signal_q = signal_q, num_vals = num_vals,screen_width = size.width(), screen_height = size.height(), monitors = test_mons)
    app.exec_()
    input("press a key then enter to exit")
    #close all the processes
    _threadManager_.terminate()
    _threadManager_.join()
