#we want to test multiprocessing with basic output plotting update, and generating new values in the plots/display
#this works so far for plotting on the main thread, we could set up a shared memory space to try putting the plotting
#thread on to a seperate thread, but matplotlib will likely not appreciate that
import socket
import pandas as pd
import numpy as np
import time
from matplotlib import pyplot as plt
#import can
#import RPi.GPIO as GPIO
import os
import multiprocessing as mp
global lines, backgrounds,fig,axs
import sys
import matplotlib
from PyQt5 import QtCore, QtWidgets
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg
from matplotlib.figure import Figure
from matplotlib.backends.backend_qt5agg import FigureCanvas
import matplotlib.animation as anim

def gen_value(vals,q):
    while True:
        t_sleep = np.random.rand() #generate random sleep time
        time.sleep(t_sleep/4)#randomly wait
        val = np.random.rand()*3
        #print("from thread "+str(vals)+" value " + str(val))
        q.put([vals,val])#send id and value

def update_canvas():
    global vals_arr
    recv_vals = []
    while not q.empty():
        recv_vals.append(q.get())
        print(recv_vals)
    for val in recv_vals:
        i = val[0]
        x = val[1]
        vals_arr[i].append(x)
        print(x)

    max_len = 100
    for i in range(num_vals):
        if len(vals_arr[i]) > 100:
            diff = len(vals_arr[i])-100
            vals_arr[i] = vals_arr[i][diff:]

        lines.set_data(range(len(vals_arr[i])),vals_arr[i])
    return lines

def init_anim():
    axs.set_rmax(3)
    axs.set_rticks([1,2,3])

    return lines,
if __name__ == "__main__":
    pipes = []
    procs = []  #send processes

    q = mp.Queue()
    num_vals = 1

    val_gen = mp.Process( target = gen_value, args = (1,q))
    val_gen.start()
    procs.append(val_gen)
    #we need to get values from the queue
    time.sleep(1)# let the program start
    figure,   axs = plt.subplots(subplot_kw = {'projection':'polar'})
    lines = []
    lines.append(axs.plot([],lw = 1)) #of the form theta, radius


    interval = .05
    vals_arr = []
    func = anim.FuncAnimation(fig = figure,init_func= init_anim, func = update_canvas, fargs = (), interval = interval , blit = True, repeat = False)
    plt.show()

    input("press a key then enter to exit")
    #close all the processes
    for p in procs:
        p.terminate()
        p.join()
