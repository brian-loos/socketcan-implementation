import socket
import pandas as pd
import numpy as np
import time
from matplotlib import pyplot as plt
#import can
#import RPi.GPIO as GPIO
import os
import multiprocessing as mp
global lines, backgrounds,fig,axs
import sys
import matplotlib
matplotlib.use('Qt5Agg')

from PyQt5 import QtCore, QtWidgets
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg
from matplotlib.figure import Figure

def gen_value(vals,q):
    while True:
        t_sleep = np.random.rand() #generate random sleep time
        time.sleep(t_sleep)#randomly wait
        val = np.random.randint(0,10)
        #print("from thread "+str(vals)+" value " + str(val))
        q.put([vals,val])#send id and value
def plotting(num_vals,q):
    fig,axs = plt.subplots(num_vals,1)
    fig.tight_layout()
    lines = []
    for i in range(num_vals):
        lines.append(axs[i].plot([],lw=1))
        crit_val = np.random.randint(3,8)
        axs[i].plot([0,99],[crit_val,crit_val], color = 'red')
        axs[i].set_ylim(0,10)
        axs[i].set_xlim(0,100)
        axs[i].set_title('blah')
        axs[i].set_xlabel('values')
    fig.canvas.draw()
    backgrounds = []
    for i in range(num_vals):
        backgrounds.append(fig.canvas.copy_from_bbox(axs[i].bbox))
    vals_arr = []
    for i in range(num_vals):
        vals_arr.append([])
    #init_plots(num_vals)
    recv_vals = []
    plt.show(block = False)
    #print(lines[1][0])
    while True:
        recv_vals = []

        while not q.empty():
            recv_vals.append(q.get())#get the values and append to a list
        #print(recv_vals)#need to sort this into a recieved value array
        #if len(recv_vals) > 1:
        #    recv_vals = sort_vals(recv_vals)
        for val in recv_vals:
            i = val[0]
            x = val[1]
            vals_arr[i].append(x)
        #print(vals_arr)
        max_len = 100
        for i in range(num_vals):
            if len(vals_arr[i]) > 100:
                diff = len(vals_arr[i])-100
                vals_arr[i] = vals_arr[i][diff:]

            lines[i][0].set_data(range(len(vals_arr[i])),vals_arr[i])

            #axs[i].set_xlim(max(times)-10,max(times))  #active rescale
            fig.canvas.restore_region(backgrounds[i])
            axs[i].draw_artist(lines[i][0])
            fig.canvas.blit(axs[i].bbox)

if __name__ == "__main__":
    q= mp.Queue()
    procs =[]
    num_vals = 4
    for vals in range(num_vals):#need to create a set of pipes for these processes and we need to set up all the processes
        #don't need pipes for this implementation
        #tx_conn,rx_conn = mp.Pipe()
        #pipes.append([tx_conn,rx_conn])
        procs.append(mp.Process(target = gen_value,args =(vals,q)))

    #start processes
    for p in procs:
        p.start()
    time.sleep(1)# let the program start
    plot_job = mp.Process(target = plotting, args = (num_vals,q))
    plot_job.start()
    #plotting(num_vals,q)
    procs.append(plot_job)
    input("press a key then enter to exit")
    #close all the processes
    for p in procs:
        p.terminate()
        p.join()
