#we want to test multiprocessing with basic output plotting update, and generating new values in the plots/display
#this works so far for plotting on the main thread, we could set up a shared memory space to try putting the plotting
#thread on to a seperate thread, but matplotlib will likely not appreciate that
import socket
import pandas as pd
import numpy as np
import time
from matplotlib import pyplot as plt
#import can
#import RPi.GPIO as GPIO
import os
import multiprocessing as mp
global lines, backgrounds,fig,axs
import sys
import matplotlib
from PyQt5 import QtCore, QtWidgets
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg
from matplotlib.figure import Figure
from matplotlib.backends.backend_qt5agg import FigureCanvas
import matplotlib.animation as anim
global q,num_vals

matplotlib.use('Qt5Agg')
#working framework for plotting animations
class AnimFigureCanvas(FigureCanvasQTAgg, anim.FuncAnimation):
    #This class takes in an active queue, q (from multiprocessing) and the number of values that are going to be plotted
    #we can add another way to specify the location from which the values are coming from
    def __init__(self,q,num_vals)-> None:
        super().__init__(Figure())
        self._q_ = q #need access to the queue
        self._num_vals_ = num_vals

        self.axs = self.figure.subplots(self._num_vals_,1)
        self._lines_ = []


        #need to update this to accomodate for different sources
        for i in range(self._num_vals_):

            self._lines_.append(self.axs[i].plot([],lw = 1)[0])
            crit_val = np.random.rand()
            self.axs[i].plot([0,99],[crit_val,crit_val], color = 'red')
            self.axs[i].set_ylim(-.5,2)
            self.axs[i].set_xlim(0,10)
            self.axs[i].set_title('blah',loc = 'right')
            self.axs[i].set_xlabel('values')
            self.axs[i].set_xticks(list(range(0,10)))

            self.axs[i].set_xticklabels(list(range(-10,0)))

        interval = .01# we want to update as quick as possible, probably
        self.vals_arr = []
        self._times_ = []
        for i in range(self._num_vals_):
            self.vals_arr.append([.5])#init value
            self._times_.append([0.])
        #self._timer_ = self.new_timer(interval, [(self._update_canvas_, (), {})])
        #self._timer_.start()
        self._tstart_ = time.time()#start time
        anim.FuncAnimation.__init__(self,self.figure, self._update_canvas_, fargs=(), interval=interval, blit=True,repeat = False)
        return
    def _update_canvas_(self,i)-> None:
        #This is the update method for the animation framework running the graph
        #get current time
        num_new_vals = np.zeros(self._num_vals_) #init to zero
        recv_vals = []
        while not self._q_.empty():
            recv_vals.append(self._q_.get())#get the values and append to a list
        for i in range(self._num_vals_):
            num_new_vals[i] = len(self.vals_arr[i])#init value
        for val in recv_vals:
            i = val[0]
            x= val[1]
            self.vals_arr[i].append(x+ self.vals_arr[i][len(self.vals_arr[i])-1])
        self._tcurr_ = time.time()-self._tstart_#get current time
        for i in range(self._num_vals_):
            num_new_vals[i] = len(self.vals_arr[i])-num_new_vals[i]
            #need to know if this is nonzero
            if num_new_vals[i] != 0:
                end_val = self._times_[i][len(self._times_[i])-1]
                dt = self._tcurr_-end_val
                self._times_[i].extend(list(np.linspace(end_val+dt/num_new_vals[i], self._tcurr_, int(num_new_vals[i]))))#add values to times

        cutoff = 10
        max_len = 10000
        for i in range(self._num_vals_):
            if self._times_[i][len(self._times_[i])-1] > cutoff:
                diff = int(num_new_vals[i])
                self.vals_arr[i] = self.vals_arr[i][diff:]
                self._times_[i] = self._times_[i][diff:]
                self.axs[i].set_xlim(self._times_[i][0],self._times_[i][len(self._times_[i])-1])
            #    self.axs[i].set_xticklabels(list(np.linspace(self._times_[i][0],self._times_[i][len(self._times_[i])-1],10)))
            #self.axs[i].clear()
            #self.axs[i].set_ylim(0,10)
            #self.axs[i].set_xlim(0,100)
            #self.axs[i].plot(range(len(self.vals_arr[i])),self.vals_arr[i])
            #print(self._lines_[i])
            #print(self._lines_[i][0])
            self._lines_[i].set_xdata(self._times_[i])
            self._lines_[i].set_ydata(self.vals_arr[i])
        self._t0_ = time.time()
        #self.draw()
        #self.show()
        return self._lines_

class PolarFigureCanvas(FigureCanvasQTAgg, anim.FuncAnimation):
    #This class takes in an active queue, q (from multiprocessing) and the number of values that are going to be plotted
    #we can add another way to specify the location from which the values are coming from
    def __init__(self)-> None:
        super().__init__(Figure())

        self.axs = self.figure.subplots(subplot_kw = {'projection':'polar'})
        self._lines_ = []

        self._theta_ = 0
        self._lines_.append(self.axs.plot([],lw = 4)[0])
        self._lines_[0].set_data([0,self._theta_],[0,2])
        self.axs.set_rmax(3)
        self.axs.set_rticks([1,2,3])
        self.axs.fill_between(np.linspace(np.pi, np.pi*3/2, 100),0,3,)

        interval = .01# we want to update as quick as possible, probably
        anim.FuncAnimation.__init__(self,self.figure, self._update_canvas_, fargs=(), interval=interval, blit=True,repeat = False)
        return
    def _update_canvas_(self,i)-> None:
        self._theta_ = self._theta_ + self.get_next_val(i)
        self._lines_[0].set_data([0,self._theta_],[0,2])
        return self._lines_
    def get_next_val(self,i):
        return (np.random.rand()-.5)*.05

class MainWindow(QtWidgets.QMainWindow):

    def __init__(self,q, num_vals,screen_width, screen_height, *args, **kwargs):
        super(MainWindow, self).__init__(*args, **kwargs)
        sizeObject = QDesktopWidget().screenGeometry(-1)
        self.q = q
        self.num_vals = num_vals
        self._width_ = screen_width
        self._height_ = screen_height
        self.setupUi()
        # Create the maptlotlib FigureCanvas object,
        # which defines a single set of axes as self.axes.
        self.show()
    def setupUi(self):
        self.setWindowTitle('Plotting thingy')

        self.resize(self._width_, self._height_)
        self.move(0,0)
        wid = QWidget(self) #create central widget
        self.setCentralWidget(wid)
        layout = QGridLayout()#create the grid layout
        wid.setLayout(layout)#this layout is the layout for our program
        push_button = QPushButton()
        push_button.setText('fuck you')
        push_button.setFont(QFont('Lucidia Console', 16))
        push_button.clicked.connect(self.fuck_you)
        layout.addWidget(push_button,0,0)
        #sc = MplCanvas(self, self.q, width=5, height=4, dpi=100,num_vals = 4)
        sc = AnimFigureCanvas(self.q, self.num_vals)
        rp = PolarFigureCanvas()
        #sc = TestClass()
        #need to run a modified version of the plotting method
        layout.addWidget(sc,0,1,3, 3)
        layout.addWidget(rp,1,0,2,1)
        self.show()
        #sc.run(4,self.q)


    def fuck_you(self):
        text, ok = QInputDialog.getText(self, 'Fuck you', 'Big fuck you')
    def sort_vals(lst):
        i = 0
        l = len(lst)
        while True:

            if lst[i][0] > lst[i+1][0]: #swap places
                temp = lst[i+1]
                lst[i+1] = lst[i]
                lst[i] = temp
                if i == 0:
                    i += 1
                else:
                    i += -1
            else:#no swap move on
                i += 1

            if i == l-1:
                break
        return lst#inplace sort

num_vals = 3 #4 seperate test routines

#basically need to add a method here to fetch values using socketCAN rather than generating random values
def gen_value(vals,q):
    while True:
        t_sleep = np.random.rand() #generate random sleep time
        time.sleep(t_sleep/20)#randomly wait
        #val = np.random.randint(0,10)
        val = (np.random.rand() -.5)/25#small value
        #print("from thread "+str(vals)+" value " + str(val))
        q.put([vals,val])#send id and value

if __name__ == "__main__":
    pipes = []
    procs = []  #send processes

    q = mp.Queue()

    for vals in range(num_vals):#need to create a set of pipes for these processes and we need to set up all the processes
        #don't need pipes for this implementation
        #tx_conn,rx_conn = mp.Pipe()
        #pipes.append([tx_conn,rx_conn])
        procs.append(mp.Process(target = gen_value,args =(vals,q)))

    #start processes
    for p in procs:
        p.start()

    #we need to get values from the queue
    time.sleep(1)# let the program start
    app = QtWidgets.QApplication(sys.argv)
    screen = app.primaryScreen()
    size = screen.size()

    w = MainWindow(q = q, num_vals = num_vals,screen_width = size.width(), screen_height = size.height())
    app.exec_()
    input("press a key then enter to exit")
    #close all the processes
    for p in procs:
        p.terminate()
        p.join()
